package fr.upsilon.spotify.model.repository

import fr.upsilon.spotify.model.entities.Album
import fr.upsilon.spotify.model.services.AlbumService
import fr.upsilon.spotify.model.services.ApiConnection
import retrofit2.await

object AlbumRepository {
    suspend fun getAlbumRank(): List<Album> {
        val rankTracks = ApiConnection.connection().create(AlbumService::class.java).listAlbumsRank().await()
        return rankTracks.listLoved
    }
}