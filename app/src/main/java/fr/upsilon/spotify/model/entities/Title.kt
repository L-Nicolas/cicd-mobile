package fr.upsilon.spotify.model.entities

import java.io.Serializable

data class Title(
    val id: String? = null,
    val title: String? = null
) : Serializable