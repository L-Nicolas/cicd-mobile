package fr.upsilon.spotify.model.entities

data class AlbumData (
    val idArtist: String,
    val strAlbum: String,
    val strGenre: String,
    val strArtist:String,
    val intYearReleased: String,
    val strAlbumThumb: String,
    val intScore: String,
    val intScoreVotes: String,
    val strDescriptionEN: String
)