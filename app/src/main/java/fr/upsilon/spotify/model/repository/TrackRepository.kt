package fr.upsilon.spotify.model.repository

import fr.upsilon.spotify.model.entities.Track
import fr.upsilon.spotify.model.services.ApiConnection
import fr.upsilon.spotify.model.services.TrackService
import retrofit2.await

object TrackRepository {
    suspend fun getTrackRank(): List<Track> {
        val rankTracks = ApiConnection.connection().create(TrackService::class.java).listTracksRank().await()
        return rankTracks.listLoved
    }
}