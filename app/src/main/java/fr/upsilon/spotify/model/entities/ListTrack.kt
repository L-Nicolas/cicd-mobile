package fr.upsilon.spotify.model.entities

import com.google.gson.annotations.SerializedName

data class ListTrack (
    @SerializedName("track")
    val content: List<TrackData>
)