package fr.upsilon.spotify.model.entities

import com.google.gson.annotations.SerializedName

data class ListAlbum(
    @SerializedName("album")
    val content: List<AlbumData>,
)