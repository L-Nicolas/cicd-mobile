package fr.upsilon.spotify.model.entities

data class TrackData (
    val idTrack: String,
    val strTrack: String,
    val intTrackNumber: String
)