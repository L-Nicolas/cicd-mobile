package fr.upsilon.spotify.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.upsilon.spotify.Resource
import fr.upsilon.spotify.model.entities.Track
import fr.upsilon.spotify.model.repository.TrackRepository
import kotlinx.coroutines.launch

class TrackViewModel : ViewModel() {

    val _tracks = MutableLiveData<Resource<List<Track>>>()

    fun getTracksRank() {
        _tracks.value = Resource.Loading()
        viewModelScope.launch {
            _tracks.value = Resource.Success(TrackRepository.getTrackRank())
        }
    }

}