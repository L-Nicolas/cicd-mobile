package fr.upsilon.spotify.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.upsilon.spotify.Resource
import fr.upsilon.spotify.model.entities.Album
import fr.upsilon.spotify.model.repository.AlbumRepository
import kotlinx.coroutines.launch

class AlbumViewModel : ViewModel() {

    val _albums = MutableLiveData<Resource<List<Album>>>()

    fun getAlbumsRank() {
        _albums.value = Resource.Loading()
        viewModelScope.launch {
            _albums.value = Resource.Success(AlbumRepository.getAlbumRank())
        }
    }

}
