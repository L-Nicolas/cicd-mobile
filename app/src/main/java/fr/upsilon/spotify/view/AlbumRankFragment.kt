package fr.upsilon.spotify.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import fr.upsilon.spotify.R
import fr.upsilon.spotify.Resource
import fr.upsilon.spotify.adapter.ListAlbumAdapter
import fr.upsilon.spotify.viewmodel.AlbumViewModel


class AlbumRankFragment : Fragment() {
    private lateinit var sfl: ShimmerFrameLayout
    private lateinit var rv: RecyclerView
    private lateinit var viewModel: AlbumViewModel
    private var viewManager = GridLayoutManager(this.context, 1)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_album_rank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sfl = view.findViewById(R.id.shimmer_layout)
        rv = view.findViewById(R.id.recyclerview)

        sfl.visibility = View.VISIBLE

        viewModel = ViewModelProvider(this)[AlbumViewModel::class.java]
        rv.layoutManager = viewManager
        observeData()

        viewModel.getAlbumsRank()
    }

    @SuppressLint("UseRequireInsteadOfGet")
    fun observeData() {
        viewModel._albums.observe(viewLifecycleOwner, Observer{ status ->
            when(status) {
                is Resource.Loading<*> -> {
                    sfl.visibility = View.VISIBLE
                }
                is Resource.Error<*> -> {
                    sfl.visibility = View.VISIBLE
                }
                is Resource.Success<*> -> {
                    status.data?.let {
                        rv.adapter = ListAlbumAdapter(it)
                        rv.adapter?.notifyDataSetChanged()
                        sfl.visibility = View.GONE
                    }
                }
                else -> {
                    sfl.visibility = View.VISIBLE
                }
            }
        })
    }

}
