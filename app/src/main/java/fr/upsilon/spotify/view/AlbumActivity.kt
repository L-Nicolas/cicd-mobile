package fr.upsilon.spotify.view

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.upsilon.spotify.MainActivity
import fr.upsilon.spotify.R
import fr.upsilon.spotify.adapter.TitleAdapter
import fr.upsilon.spotify.model.entities.AlbumData
import fr.upsilon.spotify.model.entities.Title
import fr.upsilon.spotify.model.services.AlbumService
import fr.upsilon.spotify.model.services.ApiConnection
import fr.upsilon.spotify.model.services.TrackService
import kotlinx.coroutines.launch
import retrofit2.HttpException

class AlbumActivity : AppCompatActivity() {
    private lateinit var ivAlbum : ImageView
    private lateinit var ivFavLogo : ImageView
    private lateinit var ivBackLogo : ImageView
    private lateinit var ivBackground : ImageView
    private lateinit var tvArtistName : TextView
    private lateinit var tvName : TextView
    private lateinit var tvNote : TextView
    private lateinit var tvVotes : TextView
    private lateinit var tvDesc : TextView
    private lateinit var tvInfo : TextView
    private lateinit var rvList : RecyclerView
    private lateinit var albumId : String
    private var trackList = arrayListOf<Title>()
    private var viewManager = LinearLayoutManager(this@AlbumActivity)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album)
        completeXml()

        ivBackLogo.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }

        ivFavLogo.setOnClickListener {
            ivFavLogo.setImageResource(R.drawable.ic_like_on)
        }

        albumId = intent.getStringExtra("id")!!

        loadAlbum()
        loadTitles()
    }

    fun completeXml() {
        ivAlbum = findViewById(R.id.albumImage)
        ivBackground = findViewById(R.id.albumImageBackground)
        tvArtistName = findViewById(R.id.artistName)
        tvName = findViewById(R.id.name)
        tvNote = findViewById(R.id.note)
        tvVotes = findViewById(R.id.votes)
        tvDesc = findViewById(R.id.description)
        tvInfo = findViewById(R.id.info)
        rvList = findViewById(R.id.list)
        ivBackLogo = findViewById(R.id.backLogo)
        ivFavLogo = findViewById(R.id.favorisLogo)
    }

    fun completeValXml(albm: AlbumData) {
        Picasso.get().load(albm.strAlbumThumb).into(ivAlbum);
        Picasso.get().load(albm.strAlbumThumb).into(ivBackground);

        tvArtistName.text = albm.strArtist
        tvName.text = albm.strAlbum
        tvNote.text = albm.intScore

        if(albm.intScoreVotes == "null"){
            tvVotes.text = "Aucun vote"
        }else{
            tvVotes.text = albm.intScoreVotes + " votes"
        }

        tvDesc.text = albm.strDescriptionEN
    }

    fun loadAlbum() {
        lifecycleScope.launch{
            try {
                val data = ApiConnection.connection().create(AlbumService::class.java).getAlbumByIDDataAsync(albumId).await()
                completeValXml(data.content[0])
            } catch (e: HttpException) {
                Toast.makeText(this@AlbumActivity,e.toString(),Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun loadTitles() {
        lifecycleScope.launch{
            try {
                val data = ApiConnection.connection().create(TrackService::class.java).getTrackByAlbumIdDataAsync(albumId).await()
                data.content.forEach {
                    trackList.add(
                        Title(it.idTrack, it.strTrack)
                    )
                }
                rvList.layoutManager = viewManager
                rvList.adapter = TitleAdapter(trackList,this@AlbumActivity)
                tvInfo.text = trackList.count().toString() + " chansons"
            } catch (e: HttpException) {
                Toast.makeText(this@AlbumActivity,e.toString(),Toast.LENGTH_SHORT).show()
            }
        }
    }
}