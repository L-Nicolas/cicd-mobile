package fr.upsilon.spotify

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import fr.upsilon.spotify.adapter.ListTrackAdapter
import fr.upsilon.spotify.viewmodel.TrackViewModel

class TrackRankFragment : Fragment() {
    private lateinit var sfl: ShimmerFrameLayout
    private lateinit var rv: RecyclerView
    private lateinit var viewModel: TrackViewModel
    private var viewManager = GridLayoutManager(this.context, 1)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_track_rank, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sfl = view.findViewById(R.id.shimmer_layout)
        rv = view.findViewById(R.id.recyclerview)

        sfl.visibility = View.VISIBLE

        viewModel = ViewModelProvider(this)[TrackViewModel::class.java]
        rv.layoutManager = viewManager
        observeData()
        viewModel.getTracksRank()
    }

    @SuppressLint("UseRequireInsteadOfGet")
    fun observeData() {
        viewModel._tracks.observe(viewLifecycleOwner, Observer{ status ->
            when(status) {
                is Resource.Loading<*> -> {
                    sfl.visibility = View.VISIBLE
                }
                is Resource.Error<*> -> {
                    sfl.visibility = View.VISIBLE
                }
                is Resource.Success<*> -> {
                    status.data?.let {
                        Log.i("alors",it.toString())
                        rv.adapter = ListTrackAdapter(it)
                        rv.adapter?.notifyDataSetChanged()
                        sfl.visibility = View.GONE
                    }
                }
                else -> {
                    sfl.visibility = View.VISIBLE
                }
            }
        })
    }
}