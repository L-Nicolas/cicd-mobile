package fr.upsilon.spotify.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.upsilon.spotify.R
import fr.upsilon.spotify.model.entities.Album
import fr.upsilon.spotify.view.AlbumActivity

class ListAlbumAdapter (
    val albm: List<Album>
) : RecyclerView.Adapter<ListAlbumAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvRank = view.findViewById<TextView>(R.id.rank)
        val tvTitle = view.findViewById<TextView>(R.id.title)
        val tvArtist = view.findViewById<TextView>(R.id.artist)
        val ivThumb = view.findViewById<ImageView>(R.id.thumb)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAlbumAdapter.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_rank, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListAlbumAdapter.ViewHolder, position: Int) {
        val current = albm[position]

        holder.tvRank.text = (position+1).toString()
        holder.tvTitle.text = current.name
        holder.tvArtist.text = current.artist
        Picasso.get().load(current.thumb).into(holder.ivThumb)

        holder.itemView.setOnClickListener {
            val context = holder.itemView.context
            val intent = Intent(context, AlbumActivity::class.java)
            intent.putExtra("id",albm[position].id.toString())
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return albm.size
    }

}