package fr.upsilon.spotify.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import fr.upsilon.spotify.R
import fr.upsilon.spotify.model.entities.Track

class ListTrackAdapter (
    var tracks: List<Track>
) : RecyclerView.Adapter<ListTrackAdapter.ViewHolder>() {

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvRank = view.findViewById<TextView>(R.id.rank)
        val tvTitle = view.findViewById<TextView>(R.id.title)
        val tvArtist = view.findViewById<TextView>(R.id.artist)
        val ivThumb = view.findViewById<ImageView>(R.id.thumb)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTrackAdapter.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.item_rank, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ListTrackAdapter.ViewHolder, position: Int) {
        val current = tracks[position]

        holder.tvRank.text = (position+1).toString()
        holder.tvTitle.text = current.title
        holder.tvArtist.text = current.artist
        Picasso.get().load(current.thumb).into(holder.ivThumb)
    }

    override fun getItemCount(): Int {
        return tracks.size
    }

}