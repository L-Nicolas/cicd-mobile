package fr.upsilon.spotify.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import fr.upsilon.spotify.MainActivity
import fr.upsilon.spotify.R
import fr.upsilon.spotify.model.entities.Title

class TitleAdapter (
    private val titles: List<Title>,
    val context: Context
) : RecyclerView.Adapter<TitleAdapter.ViewHolder>(){

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.findViewById<TextView>(R.id.titreCell)
        val tvIndex = view.findViewById<TextView>(R.id.indexCell)
        var titleID = ""
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitleAdapter.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.titles_cell, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: TitleAdapter.ViewHolder, position: Int) {
        val current = titles[position]

        holder.tvTitle.text = current.title.toString()
        holder.tvIndex.text = (position+1).toString()

        holder.tvTitle.setOnClickListener {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra("idTitle", holder.titleID)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return titles.size
    }
}